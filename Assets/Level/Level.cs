﻿using UnityEngine;
using System.Collections;

public class Level : MonoBehaviour
{


    // Use this for initialization
    void Start()
    {
        GameManager.GetInstance().ResetScore();
    }

    // Update is called once per frame
    void Update()
    {
        GameManager.GetInstance().Update();
    }
}
