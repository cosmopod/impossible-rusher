﻿using UnityEngine;
using System.Collections;

public class GameManager
{


    #region GameStates
    public GameStates gm;
    #endregion

    #region Singleton
    private static GameManager instance = null;

    public static GameManager GetInstance()
    {
        if (instance == null)
        {
            instance = new GameManager();
        }

        return instance;
    }
    #endregion


    public void Start()
    {
        ResetScore();
    }

    public void Update()
    {

    }

    #region ScoreManager

    public Points points = ScriptableObject.CreateInstance<Points>();

    public void ResetScore()
    {
        if (points != null) points.ResetScore();
    }

    public void IncreaseScore()
    {
        if (points != null) points.IncreaseScore();
    }

    public int GetScore()
    {
        int score = 0;
        if (points != null) score =  points.Score;
        return score;
    }

    #endregion
}
