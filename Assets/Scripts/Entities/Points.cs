﻿using UnityEngine;
using System.Collections;

public class Points : ScriptableObject
{

    private int score = 0;

    #region GetterAnsSetters
    public int Score
    {
        get { return this.score; }
        set { this.score = value; }
    }
    #endregion

    #region ScoreManagement
    /// <summary>
    /// Adds one point to the score
    /// </summary>
    public void IncreaseScore()
    {
        Score++;
    }

    /// <summary>
    /// Resets the score to 0 points
    /// </summary>
    public void ResetScore()
    {
        Score = 0;
    }
    #endregion
}
