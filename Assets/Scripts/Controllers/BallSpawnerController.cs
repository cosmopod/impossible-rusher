﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class BallSpawnerController : MonoBehaviour
{
   
    
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    #region pooling

    public GameObject ball;
    public List<GameObject> ballList = new List<GameObject>();

    /// <summary>
    /// Get a ball prefab from the list
    /// </summary>
    /// <returns></returns>
    public GameObject GetBall()
    {
        GameObject ball = null;
        if(ballList.Count > 0)
        {
            ball = ballList[ballList.Count - 1];
            ballList.Remove(ball);

            ball.SetActive(true);
            ball.transform.position = transform.position;
            ball.transform.rotation = Quaternion.identity;
        }
        else
        {
            ball = Instantiate(this.ball, transform.position, Quaternion.identity) as GameObject;
        }

        BallController bc = ball.GetComponent<BallController>();
        if (bc != null) bc.SetColor();

        return ball;
    }

    /// <summary>
    /// Stores a ball prefab in the list
    /// </summary>
    /// <param name="ball"></param>
    public void RecycleBall(GameObject ball)
    {
        
        ballList.Add(ball);
        ball.SetActive(false);
    }

    #endregion


    #region SpawnBall


    #endregion
}
