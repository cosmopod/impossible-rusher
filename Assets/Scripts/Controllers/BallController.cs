﻿using UnityEngine;
using System.Collections;

public class BallController : MonoBehaviour
{

    public float speed = 5f;
    float increaseFactor = 1f;


    #region GettersAnsSetters
    /// <summary>
    /// Returns the final Color of the tile
    /// </summary>
    public Tiles FinalColor
    {
        get { return finalColor; }
    }
    #endregion

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        int points = GameManager.GetInstance().GetScore();
        increaseFactor = Mathf.Log(Mathf.Clamp(points, 10, int.MaxValue), 10f);
        transform.Translate(-Vector2.up * speed * increaseFactor * Time.deltaTime);
    }

    #region SetColor

    public Tiles finalColor;
    Tiles[] colors = { Tiles.Blue, Tiles.Green, Tiles.Red, Tiles.Yellow };

    /// <summary>
    /// Sets the sprite renderer color of the ball filling
    /// </summary>
    /// <param name="sr"></param>
    public void SetColor()
    {
        SpriteRenderer sr = GetComponentsInChildren<SpriteRenderer>()[1];
        if (sr != null)
        {
            int rndTileColor = Random.Range(0, colors.Length);
            finalColor = colors[rndTileColor];
            switch (finalColor)
            {
                case Tiles.Blue:
                    sr.color = Color.blue;
                    break;
                case Tiles.Red:
                    sr.color = Color.red;
                    break;
                case Tiles.Green:
                    sr.color = Color.green;
                    break;
                case Tiles.Yellow:
                    sr.color = Color.yellow;
                    break;
                default:
                    break;
            }
        }
    }

    #endregion
}
