﻿using UnityEngine;
using System.Collections;

public class TileController : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        Points = ScriptableObject.CreateInstance<Points>();
        
    }

    // Update is called once per frame
    void Update()
    {

    }


    #region MatchColor
    public Tiles tileColor;

    public bool MatchColor(Tiles tileColor)
    {
        return tileColor == this.tileColor;
    }

    #endregion

    #region BallRecycle
    public LayerMask layerMask;
    public Points Points;

    /// <summary>
    /// Sends a message to the parent in order to recycle the ball in scene and await for a new ball
    /// </summary>
    void OnTriggerEnter2D(Collider2D other)
    {
        if ((layerMask.value & 1 << other.gameObject.layer) > 0)
        {
            SendMessageUpwards("RecycleBall", other.gameObject);
            BallController bc = other.gameObject.GetComponent<BallController>();
            if (bc != null)
            {
                if (MatchColor(bc.FinalColor))
                {
                    GameManager.GetInstance().IncreaseScore();
                    
                }
                else
                {
                    Debug.Log("Wrong Tile!");
                }
            }
        }
    }

    #endregion
}
