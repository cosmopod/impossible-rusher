﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ScoreController : MonoBehaviour
{
    Text score;
    // Use this for initialization
    void Start()
    {
        score = GetComponent<Text>();
        score.text = GameManager.GetInstance().GetScore().ToString();
    }

    // Update is called once per frame
    void Update()
    {
        score.text = GameManager.GetInstance().GetScore().ToString();
    }
}
