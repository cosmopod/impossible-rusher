﻿using UnityEngine;
using System.Collections;

public class SquareController : MonoBehaviour
{
    bool isRotating = false;
    Quaternion newRotation;

    // Use this for initialization
    void Start()
    {
        isWaitingBall = true;

    }

    // Update is called once per frame
    void Update()
    {
        if (isWaitingBall)
        {
            if (ballSpawner != null) ballSpawner.GetBall();
            isWaitingBall = false;
        }



        if (Input.GetButtonDown("Jump") && !isRotating)
        {
            isRotating = true;
            newRotation = transform.rotation * Quaternion.Euler(0f, 0f, -90f);
        }
        if (isRotating)
        {
            RotateSquare(newRotation);
        }

    }

    #region Rotation

    public float rotationSpeed = 150f;

    /// <summary>
    /// Applies a rotation to the square
    /// </summary>
    /// <param name="newRotation"></param>
    void RotateSquare(Quaternion newRotation)
    {
        
        if (isRotating)
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, newRotation, rotationSpeed * Time.deltaTime);
            if (transform.rotation.eulerAngles.z == newRotation.eulerAngles.z)
            {
                isRotating = false;
                transform.rotation = newRotation;
            }
        }
    }
    #endregion

    #region BallRecycle

    public BallSpawnerController ballSpawner;
    bool isWaitingBall;

    /// <summary>
    /// Recycles the ball in scene and waits for a new ball
    /// </summary>
    /// <param name="ball"></param>
    public void RecycleBall(GameObject ball)
    {
        if (ballSpawner != null) ballSpawner.RecycleBall(ball);
        isWaitingBall = true;
    }

    #endregion

}
